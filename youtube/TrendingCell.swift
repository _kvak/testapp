//
//  TrendingCell.swift
//  youtube
//
//  Created by Andre Kvashuk on 11/27/16.
//  Copyright © 2016 test. All rights reserved.
//

import UIKit

class TrendingCell: FeedCell {

    override func fetchVideos() {
        APIManager.sharedInstance.fetchTrendingFeed { (videos) in
            self.videos = videos
            self.collectionView.reloadData()
        }
    }
}
