//
//  APIManager.swift
//  youtube
//
//  Created by Andre Kvashuk on 11/25/16.
//  Copyright © 2016 test. All rights reserved.
//

import UIKit

class APIManager: NSObject {
    static let sharedInstance = APIManager()
    let baseUrl = "https://s3-us-west-2.amazonaws.com/youtubeassets"
    
    func fetchFeedFromAPI(completion: @escaping ([Video]) -> ()){
        let feedString = "/home.json"
        fetchFeedFromUrl(urlString: "\(baseUrl)\(feedString)", completion: completion)
    }
    
    func fetchTrendingFeed(completion: @escaping ([Video])->()) {
        let trendingString = "/trending.json"
        fetchFeedFromUrl(urlString: "\(baseUrl)\(trendingString)", completion: completion)
    }
    
    func fetchSubscriptionsFeed(completion: @escaping ([Video]) -> ()) {
        let subscriptionsString = "/subscriptions.json"
        fetchFeedFromUrl(urlString: "\(baseUrl)\(subscriptionsString)", completion: completion)
    }
    
    func fetchFeedFromUrl(urlString: String, completion:@escaping ([Video]) ->()) {
        let feedUrl = URL(string: urlString)
        
        URLSession.shared.dataTask(with: feedUrl! ) { (data, response, error) in
            if error != nil {
                print(error)
                return
            }
            
            do {
                if let unwrappedData = data, let jsonDictionaries = try JSONSerialization.jsonObject(with: unwrappedData, options: .mutableContainers) as? [[String : AnyObject]] {
                    
                    let videos = jsonDictionaries.map({return Video(dictionary: $0)})
                    
                    DispatchQueue.main.async(execute: {
                        completion(videos)
                    })
                }
            } catch let jsonError {
                print(jsonError)
            }

        }.resume()
    }
}



//
//do {
//    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
//    
//    for dictionary in json as! [[String: AnyObject]] {
//        
//        let video = Video()
//        video.setValuesForKeys(dictionary)
//        
//        //                    video.title = dictionary["title"] as? String
//        //                    video.thumbnailImageName = dictionary["thumbnail_image_name"] as? String
//        
//        let channelDictionary = dictionary["channel"] as! [String: AnyObject]
//        
//        let channel = Channel()
//        channel.setValuesForKeys(channelDictionary)
//        
//        video.channel = channel
//        
//        videos.append(video)
//    }
//    
//    DispatchQueue.main.async(execute: {
//        completion(videos)
//    })
//    
//} catch let jsonError {
//    print(jsonError)
//}

//little shorter {
//                    let channelDictionary = dictionary["channel"] as! [String: AnyObject]
//
//                    let channel = Channel()
//                    channel.setValuesForKeys(channelDictionary)
//
//                    video.channel = channel

//}

