//
//  FeedCell.swift
//  youtube
//
//  Created by Andre Kvashuk on 11/26/16.
//  Copyright © 2016 test. All rights reserved.
//

import UIKit

class FeedCell: BaseCell,
UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    
    var videos: [Video]?
    let cellId = "cellId"
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = .zero
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .white
        cv.delegate = self
        cv.dataSource = self
        
        return cv
    }()
    
    override func setupViews() {
        super.setupViews()
        setupCollectionView()
        
        fetchVideos()
    }
    
    func fetchVideos() {
        APIManager.sharedInstance.fetchFeedFromAPI(completion: { (videos) in
            self.videos = videos
            self.collectionView.reloadData()
        })
    }
    
    func setupCollectionView() {
        self.collectionView.register(VideoCell.self, forCellWithReuseIdentifier: cellId)
 
        collectionView.contentInset            = UIEdgeInsetsMake(50, 0, 50, 0)
        collectionView.scrollIndicatorInsets   = UIEdgeInsetsMake(50, 0, 50, 0)
    
        addSubview(collectionView)
        addConstraintsWithFormat("H:|[v0]|", views: collectionView)
        addConstraintsWithFormat("V:|[v0]|", views: collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videos?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! VideoCell
        cell.video = videos?[(indexPath as NSIndexPath).item]
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = (frame.width - 16 - 16) * 9 / 16
        
        return CGSize(width: frame.width, height: height + 16 + 88)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vl = VideoPlayerLauncher()
        vl.showVideoPlayer()
    }
}
