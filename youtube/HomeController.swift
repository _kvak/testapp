//
//  ViewController.swift
//  youtube
//
//  Created by Andre Kvashuk on 11/11/16.
//  Copyright © 2016 test. All rights reserved.
//

import UIKit

class HomeController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var videos: [Video]?
    let cellIdentifiers = ["homeFeedCell", "trendingFeedCell", "subsFeedCell", "accountCell"]
    let titles          = ["Home", "Trending", "Subscriptions", "Account"]
    
    lazy var menuBar: MenuBar = {
        let mb = MenuBar()
        mb.homeController = self
        
        return mb
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.isTranslucent = false

        setupTitleLabel()
        setupTitleForCellIndex(index: 0)
        setupCollectionView()
        setupMenuBar()
        setupNavBarButtons()
    }
    
    var itemSize: (CGSize)?
    
    func setupTitleLabel() {
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
//        titleLabel.text = "  Home"
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont.systemFont(ofSize: 20)
        navigationItem.titleView = titleLabel
    }
    
    func setupCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 0
        flowLayout.sectionInset = .zero
    
        collectionView?.collectionViewLayout    = flowLayout
        collectionView?.backgroundColor         = UIColor.white
        collectionView?.contentInset            = UIEdgeInsetsMake(50, 0, 0, 0)
        collectionView?.scrollIndicatorInsets   = UIEdgeInsetsMake(50, 0, 0, 0)
        
        collectionView?.register(FeedCell.self, forCellWithReuseIdentifier: cellIdentifiers[0])
        collectionView?.register(TrendingCell.self, forCellWithReuseIdentifier: cellIdentifiers[1])
        collectionView?.register(SubscriptionsCell.self, forCellWithReuseIdentifier: cellIdentifiers[2])
        collectionView?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellIdentifiers[3])
        
        collectionView?.frame = self.view.frame
        collectionView?.isPagingEnabled = true
        
        let width = (self.collectionView?.frame.width)!
        let height = self.view.frame.height - menuBar.frame.height
        itemSize = CGSize(width:width, height:height)
    }
    
    func setupNavBarButtons() {
        let searchImage = UIImage(named: "search_icon")?.withRenderingMode(.alwaysOriginal)
        let searchBarButtonItem = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(handleSearch))
        
        let moreButton = UIBarButtonItem(image: UIImage(named: "nav_more_icon")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMore))
        
        navigationItem.rightBarButtonItems = [moreButton, searchBarButtonItem]
    }
    
    lazy var settingLauncher: SettingsLauncher = {
       let setLauncher = SettingsLauncher()
       setLauncher.homeViewController = self
        
        return setLauncher
    }()
    
    func handleMore() {
        settingLauncher.showSettings()
    }
    
    func handleSearch() {
        print(123)
    }
    
    func scrollToMenuAt(index: Int) {
        let indexPath = IndexPath(item: index, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: [], animated: true)
    }
    
    func showDummyViewControllerForSetting(setting: Setting) {
        let title = setting.name?.rawValue
        let vc = UIViewController()
        vc.navigationItem.title = title
        vc.view.backgroundColor = UIColor.white
        
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    fileprivate func setupMenuBar() {
        navigationController?.hidesBarsOnSwipe = true
        let redView = UIView()
        redView.backgroundColor = UIColor.rgb(230, green: 32, blue: 31)
        
        view.addSubview(redView)
        view.addSubview(menuBar)
        
        view.addConstraintsWithFormat("H:|[v0]|", views: redView)
        view.addConstraintsWithFormat("V:[v0(50)]", views: redView)
        view.addConstraintsWithFormat("H:|[v0]|", views: menuBar)
        view.addConstraintsWithFormat("V:[v0(50)]", views: menuBar)
        
        menuBar.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor).isActive = true
    }
    
    func setupTitleForCellIndex(index: Int) {
        if index < titles.count {
            let titleLabel = navigationItem.titleView as? UILabel
            titleLabel?.text = "  \(titles[index])"
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4 //self.menuBar.collectionView.numberOfItems(inSection: 0)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = cellIdentifiers[indexPath.item]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.itemSize!
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        menuBar.leftAnchorSliderBarConstraint?.constant = scrollView.contentOffset.x / 4
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let x = targetContentOffset.pointee.x
        
        let index = x / view.frame.width
        self.menuBar.selectIconAt(index: Int(index))
        self.setupTitleForCellIndex(index: Int(index))
    }
    override func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        navigationController?.navigationBar.isHidden = false
    }
    
}

//    let itemSize = self.collectionView?.frame.size - self.collectionView.sectionInset




