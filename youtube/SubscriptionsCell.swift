//
//  SubscriptionsCell.swift
//  youtube
//
//  Created by Andre Kvashuk on 11/27/16.
//  Copyright © 2016 test. All rights reserved.
//

import UIKit

class SubscriptionsCell: FeedCell {

    override func fetchVideos() {
        APIManager.sharedInstance.fetchSubscriptionsFeed { (videos) in
            self.videos = videos
            self.collectionView.reloadData()
        }
    }

}
