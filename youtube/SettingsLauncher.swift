//
//  SettingsLauncher.swift
//  youtube
//
//  Created by Andre Kvashuk on 11/23/16.
//  Copyright © 2016 test. All rights reserved.
//

import UIKit


class Setting: NSObject {
    var name: SettingName?
    var imageName: String
    
    init(name: SettingName, imageName: String) {
        self.name = name
        self.imageName = imageName
        
        super.init()
    }
}

enum SettingName: String {
    case Cancel = "Cancel"
    case TermsAndPrivacy = "Terms and privacy policy"
    case SendFeedback = "Send Feedback"
    case SwitchAccount = "Switch Account"
    case Settings = "Settings"
    case Help = "Help"
    case None = ""
}

class SettingsLauncher: NSObject, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    let cellId = "settingsCellId"
    let cellHeight: CGFloat = 50
    var homeViewController : HomeController?
    
    var settings:[Setting] = {
        return [
         Setting(name: .Settings, imageName: "settings"),
         Setting(name: .TermsAndPrivacy, imageName: "privacy"),
         Setting(name: .SendFeedback, imageName: "feedback"),
         Setting(name: .SwitchAccount, imageName: "switch_account"),
         Setting(name: .Help, imageName: "help"),
         Setting(name: .Cancel, imageName: "cancel")
        ]
    }()
    
    override init() {
        super.init()
        
        self.collectioView.delegate = self
        self.collectioView.dataSource = self
        self.collectioView.register(SettingsCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    let blackView = UIView()
    let collectioView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        
        return cv
    }()
    
    func showSettings() {
        if let window = UIApplication.shared.keyWindow {
            self.blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
           
            let tapGesture = UITapGestureRecognizer()
            tapGesture.addTarget(self, action: #selector(handleDissmiss))
            
            self.blackView.addGestureRecognizer(tapGesture)
            self.blackView.isUserInteractionEnabled = true
            
            window.addSubview(blackView)
            window.addSubview(collectioView)
            self.blackView.frame = window.frame
            self.blackView.alpha = 0
            
            let menuHeight: CGFloat = CGFloat(self.settings.count) * cellHeight
            let y = window.frame.size.height - menuHeight
            self.collectioView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: menuHeight)
            
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.collectioView.frame = CGRect(x: 0, y: y, width: window.frame.size.width, height: menuHeight)
                }, completion: nil)
        }
    }
    
    func handleDissmiss(setting: Setting) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.blackView.alpha = 0
            
            if let window = UIApplication.shared.keyWindow {
                self.collectioView.frame = CGRect(x: 0, y: window.frame.height, width: self.collectioView.frame.width, height: self.collectioView.frame.height)
            }
        }) { (completed: Bool) in
            if type(of: setting) != Setting.self {
                return
            }
            if setting.name != .Cancel {
                self.homeViewController?.showDummyViewControllerForSetting(setting: setting)
            }
        }
    }
    
// MARK: - CollectionView methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.settings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectioView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SettingsCell
        
        cell.setting = self.settings[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectioView.frame.width, height: cellHeight)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let setting: Setting = self.settings[indexPath.item]
        
        handleDissmiss(setting: setting)
    }
}
