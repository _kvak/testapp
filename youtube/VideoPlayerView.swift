//
//  VideoPlayerView.swift
//  youtube
//
//  Created by Andre Kvashuk on 11/28/16.
//  Copyright © 2016 test. All rights reserved.
//

import UIKit
import AVFoundation

class VideoPlayerView: UIView  {
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        
        return activityIndicatorView
    }()
    
    lazy var pausePlayButton: UIButton = {
        let btn = UIButton(type: .system)
        let pauseImage = UIImage(named: "pause")
        btn.setImage(pauseImage, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(handlePlayPause), for: .touchUpInside)
        btn.tintColor = .white
        btn.isHidden = true
        
        return btn
    }()
    
    lazy var controlsContainerView: UIView = {
        let containerView = UIView()
        containerView.frame = self.frame
        containerView.backgroundColor = UIColor.init(white: 0, alpha: 0.5)
        
        return containerView
    }()
    
    var videoLengthLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "00:00"
        label.font = label.font.withSize(13)
        label.textColor = .white
        label.textAlignment = .right
        
        return label
    }()
    
    var videoCurrentTimeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "00:00"
        label.font = label.font.withSize(13)
        label.textColor = .white
        label.textAlignment = .left
        
        return label
    }()
    
    lazy var videoSlider: UISlider = {
        let slider = UISlider()
        slider.minimumTrackTintColor = .red
        slider.maximumTrackTintColor = .white
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.addTarget(self, action: #selector(handleSliderValueChange), for: .valueChanged)
        
        return slider
    }()
    
    var isPlaying: Bool = false
    
    func handlePlayPause() {
        if isPlaying {
            player?.pause()
            pausePlayButton.setImage(UIImage(named: "play"), for: .normal)
        } else {
            player?.play()
            pausePlayButton.setImage(UIImage(named: "pause"), for: .normal)
        }
        
        isPlaying = !isPlaying
    }
    
    func handleSliderValueChange() {
        if let duration = player?.currentItem?.duration {
            let multiple = videoSlider.value
            let seconds = CMTimeGetSeconds(duration)
            
            let seekTimeFloat = seconds * Float64(multiple)
            let seekTime = CMTime(seconds: seekTimeFloat, preferredTimescale: 1)
            player?.seek(to: seekTime)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .black
        
        setupAVPlayer()
        setupContainerView()
        activityIndicator.startAnimating()
    }
    var player: AVPlayer?
    
    func setupAVPlayer() {
        let urlString = "https://firebasestorage.googleapis.com/v0/b/gameofchats-762ca.appspot.com/o/message_movies%2F12323439-9729-4941-BA07-2BAE970967C7.mov?alt=media&token=3e37a093-3bc8-410f-84d3-38332af9c726"

        if let url = URL(string: urlString) {
            let player = AVPlayer(url: url)
            
            let videoPlayerLayer = AVPlayerLayer(player: player)
            videoPlayerLayer.frame = self.frame
            self.layer.addSublayer(videoPlayerLayer)
           
            player.play()
            player.addObserver(self, forKeyPath: "currentItem.loadedTimeRanges", options: .new, context: nil)
            self.player = player
            let interval = CMTime(seconds: 1, preferredTimescale: 1)
            player.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (timeInterval) in
                if let duration = player.currentItem?.duration, let currentTime = player.currentItem?.currentTime() {
                    let currentSeconds = CMTimeGetSeconds(currentTime)
                    let totalSeconds = CMTimeGetSeconds(duration)
                    let newSliderValue = currentSeconds / totalSeconds
                    self.videoSlider.value = Float(newSliderValue)
                    
                    let secondsText = Int(currentSeconds) % 60
                    let minutesText = Int(currentSeconds) / 60
                    self.videoCurrentTimeLabel.text = String(format: "%02d:%02d", minutesText, secondsText)
                }
            })
        }
    }
    
    func setupContainerView() {
        self.addSubview(controlsContainerView)
        
        controlsContainerView.addSubview(activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: controlsContainerView.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: controlsContainerView.centerYAnchor).isActive = true
        
        controlsContainerView.addSubview(pausePlayButton)
        pausePlayButton.centerXAnchor.constraint(equalTo: controlsContainerView.centerXAnchor).isActive = true
        pausePlayButton.centerYAnchor.constraint(equalTo: controlsContainerView.centerYAnchor).isActive = true
        pausePlayButton.heightAnchor.constraint(equalToConstant: 30)
        pausePlayButton.widthAnchor.constraint(equalToConstant: 30)
        
        controlsContainerView.addSubview(videoCurrentTimeLabel)
        videoCurrentTimeLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        videoCurrentTimeLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2).isActive = true
        videoCurrentTimeLabel.heightAnchor.constraint(equalToConstant: 24).isActive = true
        videoCurrentTimeLabel.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        controlsContainerView.addSubview(videoLengthLabel)
        videoLengthLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        videoLengthLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2).isActive = true
        videoLengthLabel.heightAnchor.constraint(equalToConstant: 24).isActive = true
        videoLengthLabel.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        controlsContainerView.addSubview(videoSlider)
        videoSlider.leftAnchor.constraint(equalTo: videoCurrentTimeLabel.rightAnchor, constant: 0).isActive = true
        videoSlider.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2).isActive = true
        videoSlider.rightAnchor.constraint(equalTo: videoLengthLabel.leftAnchor).isActive = true
        videoSlider.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        UIApplication.shared.setStatusBarHidden(true, with: .fade)
    }
    private func setupGradientLayer() {
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = frame
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradientLayer.locations = [0.7, 1.3]
        
        self.controlsContainerView.layer.addSublayer(gradientLayer)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "currentItem.loadedTimeRanges" {
            activityIndicator.stopAnimating()
            self.controlsContainerView.backgroundColor = .clear
            pausePlayButton.isHidden = false
            isPlaying = true
            
            if let duration = player?.currentItem?.duration {
                let secondsCMTimeDuration = CMTimeGetSeconds(duration)
                
                let seconds = Int(secondsCMTimeDuration) % 60
                let minutes = Int(secondsCMTimeDuration) / 60
                
                let secondsText = String(format: "%02d", seconds)
                let minutesText = String(format: "%02d", minutes)
                
                videoLengthLabel.text = "\(minutesText):\(secondsText)"
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("fatal error init coder hasn't been implemented")
    }
}

class VideoPlayerLauncher: NSObject {
    var minimizedFrame: CGRect?
    var fullScreenFrame: CGRect?
    
    
    func showVideoPlayer() {
        if let keyWindow = UIApplication.shared.keyWindow {
            let backgroundView = UIView(frame: keyWindow.frame)
            let height = backgroundView.frame.width * 9 / 16
            let videoPlayerFrame = CGRect(x: 0, y: 0, width: backgroundView.frame.width, height: height)
            let videoPlayerView = VideoPlayerView(frame: videoPlayerFrame)
            backgroundView.backgroundColor = .white
            backgroundView.addSubview(videoPlayerView)
            
            self.minimizedFrame = CGRect(x: keyWindow.frame.width - 10, y: keyWindow.frame.height - 10, width: 10, height: 10)
            backgroundView.frame = self.minimizedFrame!
            
            keyWindow.addSubview(backgroundView)
            UIView.animate(withDuration: 0.5, animations: {
                self.fullScreenFrame = keyWindow.frame
                backgroundView.frame = self.fullScreenFrame!
            })
            
        }
    }
}
