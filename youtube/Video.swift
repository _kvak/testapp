//
//  Video.swift
//  youtube
//
//  Created by Andre Kvashuk on 11/11/16.
//  Copyright © 2016 test. All rights reserved.
//

import UIKit

class SafeJsonObject: NSObject {
    override func setValue(_ value: Any?, forKey key: String) {
        let firstCapitalLetter = String(describing: key.characters.first!).capitalized
        
        
        let range = NSMakeRange(0, 1)
        
        let capitalizedKey = NSString(string: key).replacingCharacters(in: range, with: firstCapitalLetter)
        let selectorString = "set\(capitalizedKey):"
        let selector = NSSelectorFromString(selectorString)
        
        let responds = self.responds(to: selector)
        
        if !responds {
            return
        }
        
        super.setValue(value, forKey: key)
    }
}

class Video: SafeJsonObject {
    
    var thumbnail_image_name: String?
    var title: String?
    var number_of_views: NSNumber?
    var uploadDate: Date?
    var duration: NSNumber?
    
    var channel: Channel?
    
    override func setValue(_ value: Any?, forKey key: String) {
        
        if key == "channel" {
            //kvak custom channel setup
            self.channel? = Channel()
            self.channel?.setValuesForKeys(value as! [String : Any])
            
        } else {
           super.setValue(value, forKey: key) 
        }
    }
    
    init(dictionary: [String: AnyObject]) {
        super.init()
        self.setValuesForKeys(dictionary)
    }
}

class Channel: NSObject {
    var name: String?
    var profile_image_name: String?
}
